﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HZManager.Models;

namespace HZManager.ViewModels
{
    public class InventoryRecordIndexData
    {
        public IEnumerable<InventoryRecord> InventoryRecords { get; set; }
        public IEnumerable<Reason> Reasons { get; set; }
        public IEnumerable<InventoryItemRecord> InventoryItemRecord { get; set; }
    }
}