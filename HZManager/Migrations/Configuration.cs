namespace HZManager.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using HZManager.Models;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<HZManager.Models.InventoryContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HZManager.Models.InventoryContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
           var inventories = new List<Inventory>
            {
                new Inventory {PruductionID=1,Count=10},
                new Inventory {PruductionID=2,Count=11},
                new Inventory {PruductionID=3,Count=40},
                new Inventory {PruductionID=4,Count=21}
            };
            inventories.ForEach(s => context.Inventories.AddOrUpdate(p => p.PruductionID, s));
            context.SaveChanges();

            var reasons =new List<Reason>
            {
                new Reason {Name = "采购"},
                new Reason {Name = "领料"},
                new Reason {Name = "退料"},
                new Reason {Name = "新产品入库"},
                new Reason {Name = "产品发货"},
                new Reason { Name = "产品退货"}
            };
            reasons.ForEach(s=>context.Reasons.AddOrUpdate(p=>p.Name,s));
            context.SaveChanges();

         
            var inventoryRecords = new List<InventoryRecord> 
            
            {
                new InventoryRecord {Date=DateTime.Parse("2014-11-21"),Customer="孙少卿", ResponsibleOfficer="王宁",ReasonID=reasons.Single(i=>i.Name=="采购").ReasonID},
                new InventoryRecord {Date=DateTime.Parse("2014-11-22"),Customer="刘邦", ResponsibleOfficer="王宁",ReasonID=reasons.Single(i=>i.Name=="领料").ReasonID },
                new InventoryRecord {Date=DateTime.Parse("2014-11-23"),Customer="韩信", ResponsibleOfficer="王宁",ReasonID=reasons.Single(i=>i.Name=="退料").ReasonID },
                new InventoryRecord {Date=DateTime.Parse("2014-11-24"),Customer="张良", ResponsibleOfficer="王宁",ReasonID=reasons.Single(i=>i.Name=="新产品入库").ReasonID},
                new InventoryRecord {Date=DateTime.Parse("2014-11-25"),Customer="萧何", ResponsibleOfficer="王宁",ReasonID=reasons.Single(i=>i.Name=="产品发货").ReasonID},
                new InventoryRecord {Date=DateTime.Parse("2014-11-26"),Customer="吕雉", ResponsibleOfficer="王宁",ReasonID=reasons.Single(i=>i.Name=="产品退货").ReasonID }
            };
            inventoryRecords.ForEach(s => context.InventoryRecords.AddOrUpdate(p => p.InventoryRecordID, s));
            context.SaveChanges();

           var inventoryItemRecords = new List<InventoryItemRecord>
            {
                new InventoryItemRecord {InventoryRecordID=inventoryRecords.Single(d=>d.Customer=="孙少卿").InventoryRecordID,InventoryID=inventories.Single(p=>p.PruductionID==3).PruductionID ,InventoryItemCount=10,InOutInventory=1,OriginalCount=10,CurrentCount=20 }
            
            };
           inventoryItemRecords.ForEach(s => context.InventoryItemRecords.AddOrUpdate(p => p.InventoryItemRecordID, s));
           context.SaveChanges();
           
            
        }
    }
}
