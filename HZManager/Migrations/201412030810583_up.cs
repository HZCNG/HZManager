namespace HZManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class up : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InventoryRecords", "Reason_ReasonID", c => c.Int());
            CreateIndex("dbo.InventoryRecords", "Reason_ReasonID");
            AddForeignKey("dbo.InventoryRecords", "Reason_ReasonID", "dbo.Reasons", "ReasonID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InventoryRecords", "Reason_ReasonID", "dbo.Reasons");
            DropIndex("dbo.InventoryRecords", new[] { "Reason_ReasonID" });
            DropColumn("dbo.InventoryRecords", "Reason_ReasonID");
        }
    }
}
