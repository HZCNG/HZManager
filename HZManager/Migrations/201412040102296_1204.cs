namespace HZManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1204 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.InventoryRecords", "Reason_ReasonID", "dbo.Reasons");
            DropIndex("dbo.InventoryRecords", new[] { "Reason_ReasonID" });
            AddColumn("dbo.InventoryRecords", "ReasonID", c => c.Int(nullable: false));
            DropColumn("dbo.InventoryRecords", "Reason");
            DropColumn("dbo.InventoryRecords", "Reason_ReasonID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.InventoryRecords", "Reason_ReasonID", c => c.Int());
            AddColumn("dbo.InventoryRecords", "Reason", c => c.String());
            DropColumn("dbo.InventoryRecords", "ReasonID");
            CreateIndex("dbo.InventoryRecords", "Reason_ReasonID");
            AddForeignKey("dbo.InventoryRecords", "Reason_ReasonID", "dbo.Reasons", "ReasonID");
        }
    }
}
