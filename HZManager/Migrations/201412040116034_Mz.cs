namespace HZManager.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Mz : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.InventoryRecords", "ReasonID");
            AddForeignKey("dbo.InventoryRecords", "ReasonID", "dbo.Reasons", "ReasonID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InventoryRecords", "ReasonID", "dbo.Reasons");
            DropIndex("dbo.InventoryRecords", new[] { "ReasonID" });
        }
    }
}
