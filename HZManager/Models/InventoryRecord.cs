﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace HZManager.Models
{
    public class InventoryRecord
    {
        [Display(Name = "出入库编号")]
        public int InventoryRecordID { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "日期")]
        public DateTime Date { get; set; }//库存操作时间
        [Display(Name = "操作员")]
        [StringLength(50,MinimumLength=1)]
        public string Customer { get; set; }//操作工
        [Display(Name = "仓库管理员")]
        public string ResponsibleOfficer { get; set; }//仓库管理员（记录人）
       
        public int ReasonID{ get; set; }
        public virtual Reason Reason { get; set; }

        public virtual ICollection<InventoryItemRecord> InventoryItemRecords { get; set; }
    }
    public class Reason
    {
        public int ReasonID { get; set; }
        public string Name { get; set; }
        public virtual ICollection<InventoryRecord> InventoryRecords { get; set; }
    }
}