﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HZManager.Models
{
    public class InventoryItemRecord
    {
        [Display(Name="操作记录编号")]
        public int InventoryItemRecordID { get; set; }//操作主键ID
        [Display(Name="库存记录")]
        public int InventoryRecordID { get; set; }//库存记录ID
        public int InventoryID { get; set; }//库存ID
        public int InventoryItemCount { get; set; }//库存操作数量
        [Range(0,1)]
        public int InOutInventory { get; set; }//"入库or出库"
        public int OriginalCount { get; set; }//原有数量
        public int CurrentCount { get; set; }//现有数量
        public Inventory Inventory { get; set; }
       
    }
}