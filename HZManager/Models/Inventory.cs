﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HZManager.Models
{
    public class Inventory
    {
        [Display(Name="库存编号")]
        public int InventoryID { get; set; }
        [Display(Name="产品")]
        public int PruductionID { get; set; }
        [DisplayName("数量")]
        public int Count { get; set; }


    }
}