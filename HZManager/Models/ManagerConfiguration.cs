﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Infrastructure.Interception;

namespace HZManager.Models
{
    public class ManagerConfiguration:DbConfiguration
    {
        public ManagerConfiguration()
        {
            SetExecutionStrategy("System.Data.SqlClient", () => new SqlAzureExecutionStrategy());
            DbInterception.Add(new ManagerInterceptorTransientErrors());
            DbInterception.Add(new ManagerInterceptorLogging());
    }
    }
}