﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace HZManager.Models
{
    public class InventoryContext:DbContext

    {
        public InventoryContext()
            : base("InventoryContext")
        {   
         }
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<InventoryRecord> InventoryRecords { get; set; }
        public DbSet<InventoryItemRecord> InventoryItemRecords { get; set; }
        public DbSet<Reason> Reasons { get; set; }
        /*
         *  指定单一表名
         * protected override void OnModelCreating(DbModelBuilder modelBuilder)
          {
              modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

          }
      
         */
    }
}